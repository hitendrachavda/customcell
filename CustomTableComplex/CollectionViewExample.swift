//
//  CollectionViewExample.swift
//  CustomTableComplex
//
//  Created by Hitendra on 14/11/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit

class CollectionViewExample: UIViewController {

    @IBOutlet weak var collection : UICollectionView?
    
     let status : [Satus] = [.inactive,.active,.done,.inprogress,.inactive,.active,.done,.inprogress]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

}
extension CollectionViewExample: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.status.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCell", for: indexPath) as? FilterCell
        cell?.backgroundColor = UIColor(red: 31.0/255.0, green: 33.0/255.0, blue: 36.0/255.0, alpha: 1.0)
        cell!.layer.cornerRadius = 13
        cell!.layer.borderWidth = 0.5
        cell!.layer.borderColor = UIColor.white.cgColor
        var statusString = ""
        let stat = self.status[indexPath.row]
        switch stat {
        case .active:
            statusString = "Active"
        case .inprogress:
            statusString = "InProgress"
        case .done:
            statusString = "Done"
        case .inactive:
              statusString = "InActive"
        }
        cell?.lblStatus?.text = statusString
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? FilterCell
        cell?.backgroundColor = .red
        var visibleCells = collectionView.indexPathsForVisibleItems
        visibleCells = visibleCells.filter{$0 != indexPath}
        for visIndexPath in visibleCells {
            let cell = collectionView.cellForItem(at: visIndexPath)
            cell!.backgroundColor = UIColor(red: 31.0/255.0, green: 33.0/255.0, blue: 36.0/255.0, alpha: 1.0)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
}
