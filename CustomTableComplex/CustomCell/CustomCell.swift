//
//  CustomCell.swift
//  CustomTableComplex
//
//  Created by Hitendra on 13/11/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {

    
    @IBOutlet weak var lblDate : UILabel?
    @IBOutlet weak var lblTime : UILabel?
    @IBOutlet weak var lblMeetingName : UILabel?
    
    @IBOutlet weak var bgView : UIView?
    @IBOutlet weak var InfoView : UIView?
    
    @IBOutlet weak var constantHeight : NSLayoutConstraint?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.InfoView?.roundCorners([.bottomRight,.bottomLeft], radius: 5)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
