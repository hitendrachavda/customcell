//
//  Model.swift
//  CustomTableComplex
//
//  Created by Hitendra on 13/11/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import Foundation

enum Satus : Int{
   case inactive = 0
   case active
   case inprogress
   case done
}

struct Model {
    var meetingName : String
    var status : Satus
}
