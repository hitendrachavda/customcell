//
//  ViewController.swift
//  CustomTableComplex
//
//  Created by Hitendra on 13/11/20.
//  Copyright © 2020 Hitendra. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tblView : UITableView?
    @IBOutlet weak var collection : UICollectionView?
    
    let status : [Satus] = [.inactive,.active,.done,.inprogress]
    var meetings = [Model]()
    var meetingsGlobal = [Model]()
    var selectedIndex : Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for i in 1...25{
            let statusGet = status.randomElement()
            print(statusGet as Any)
            self.meetings.append(Model(meetingName: "Meeting no \(i)", status: statusGet!))
        }
        self.meetingsGlobal = self.meetings
    }
    
    @IBAction func reloadData(_ sender : UIButton){
         self.selectedIndex = -1
        self.meetings = self.meetingsGlobal
        self.tblView?.reloadData()
        self.collection?.reloadData()
    }
    
    func reloadVisibleCells(tableView : UITableView,indexPath : IndexPath){
        var visibleCells = tableView.indexPathsForVisibleRows
        visibleCells = visibleCells?.filter{$0 != indexPath}
        for index in visibleCells!{
            let cell = tableView.cellForRow(at: index) as? CustomCell
            cell?.bgView?.backgroundColor = UIColor(red: 31.0/255.0, green: 33.0/255.0, blue: 36.0/255.0, alpha: 1.0)
            cell?.bgView?.roundCorners([.allCorners], radius: 5)
            cell?.InfoView?.isHidden = true
            cell?.constantHeight?.constant = 0
        }
    }
}

extension ViewController: UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.meetings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as? CustomCell
        cell?.selectionStyle = .none
        let meeting = self.meetings[indexPath.row]
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd,yyyy"
        let date = formatter.string(from: Date())
        
        formatter.dateFormat = "hh:mm a"
        let time = formatter.string(from: Date())
        
        cell?.bgView?.backgroundColor = self.selectedIndex == indexPath.row ? .red : UIColor(red: 31.0/255.0, green: 33.0/255.0, blue: 36.0/255.0, alpha: 1.0)
        cell?.constantHeight?.constant = 0
        cell?.InfoView?.isHidden = true
        cell?.lblDate?.text = date
        cell?.lblTime?.text = time
        cell?.bgView?.roundCorners([.allCorners], radius: 5)
        cell?.lblMeetingName?.text = meeting.meetingName
        return cell!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.selectedIndex == indexPath.row ? 178 : 78
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.reloadVisibleCells(tableView: tableView, indexPath: indexPath)
        let cell = tableView.cellForRow(at: indexPath) as? CustomCell
        if(self.selectedIndex == indexPath.row){
            self.selectedIndex = -1
        }
        else{
            self.selectedIndex = indexPath.row
        }
        cell?.bgView?.backgroundColor = self.selectedIndex == indexPath.row ? .red : UIColor(red: 31.0/255.0, green: 33.0/255.0, blue: 36.0/255.0, alpha: 1.0)
        cell?.bgView?.roundCorners(self.selectedIndex == indexPath.row ? [.topLeft,.topRight] : [.allCorners], radius: 5)
        cell?.constantHeight?.constant = 78
        cell?.InfoView?.isHidden = self.selectedIndex == indexPath.row ? false : true
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}

extension ViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.status.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCell", for: indexPath) as? FilterCell
        cell?.backgroundColor = UIColor(red: 31.0/255.0, green: 33.0/255.0, blue: 36.0/255.0, alpha: 1.0)
        cell!.layer.cornerRadius = 13
        cell!.layer.borderWidth = 0.5
        cell!.layer.borderColor = UIColor.white.cgColor
        var statusString = ""
        let stat = self.status[indexPath.row]
        switch stat {
        case .active:
            statusString = "Active"
        case .inprogress:
            statusString = "InProgress"
        case .done:
            statusString = "Done"
        case .inactive:
              statusString = "InActive"
        }
        cell?.lblStatus?.text = statusString
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? FilterCell
        cell?.backgroundColor = .red
        var visibleCells = collectionView.indexPathsForVisibleItems
        visibleCells = visibleCells.filter{$0 != indexPath}
        for visIndexPath in visibleCells {
            let cell = collectionView.cellForItem(at: visIndexPath)
            cell!.backgroundColor = UIColor(red: 31.0/255.0, green: 33.0/255.0, blue: 36.0/255.0, alpha: 1.0)
        }
        let stat = self.status[indexPath.row]
        self.selectedIndex = -1
        self.meetings = self.meetingsGlobal.filter{$0.status == stat}
        self.tblView?.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
}

extension UIView{
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
      let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
      let mask = CAShapeLayer()
      mask.path = path.cgPath
      self.layer.mask = mask
    }
}
